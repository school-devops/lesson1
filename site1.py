import http.server
import socketserver

PORT = 3033

class CustomHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        with open('/var/www/site1/index.app11server1.html', 'rb') as file:
            self.wfile.write(file.read())

Handler = CustomHandler

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("Server started at localhost:" + str(PORT))
    httpd.serve_forever()
